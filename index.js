const Discord = require('discord.js');
const client = new Discord.Client();

client.on('ready', () => {
    console.log(`Bot started and is logged in as ${client.user.tag}!`);
});

client.on('message', async message => {
    if(message.content.toLowerCase() === 'Rien') {
        return;
    }
    message.channel.send("Geen Riuen");
});

client.login(process.env.TOKEN);