FROM node:15.13.0-alpine3.10

COPY . .

RUN npm install
CMD node index.js
